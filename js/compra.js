import Tarea2 from './tareas.js';

export default class ComprasPendientes1 extends Tarea2 {
    constructor(nombre, prioridad, cantidad) {
        super(nombre, prioridad);
        this.cantidad = cantidad;
    }

    mostrar(){
        super.mostrar();
        console.log(`y la cantidad de este fichero  ${this.cantidad}`);
    }

    hola(){
        return 'hola';
    }
}

