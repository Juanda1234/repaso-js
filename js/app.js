// //MODULOS EN ES6
// import { nombreTarea, crearTarea } from './tareas.js';

// //IMPORTAR UNA FUNCIÓN

// const homeWork = crearTarea('Pasear al perro', 'Media');

// console.log(nombreTarea);
// console.log(homeWork);

//EXPORTAR UNA CLASE

import Tarea5 from './tareas.js';

import ComprasPendientes1 from './compra.js';

//compras

const comprita1= new ComprasPendientes1('juanda', 'Alta', 25);
console.log('comprita clase', comprita1.mostrar());


//creamos los objetos
const tarea6 = new Tarea5('Aprender React', 'Alta');

console.log('tarea importada', tarea6.mostrar());

//variables con var
var aprendiendo = false;
console.log(aprendiendo);

//creación de variable const
//la variable constante siempre tiene que tener un valor de inicio.
//son variables que nunca cambian
const aprendiendoConst = 'javascript';

//si intentamos cambiar su valor, este no cambia.
//aprendiendoConst = true;

console.log(aprendiendoConst);

//creación de variables con let
//Es similar a var.

//scope

var musica = 'doom';

if (musica) {
    var musica = 'core';
    console.log('Dentro del if: ', musica);
}
console.log('Fuera del if: ', musica);

//scope con let: son variables de bloque.

let musica1 = 'doom';

if (musica) {
    let musica1 = 'core';
    console.log('Dentro del if: ', musica1);
}
console.log('Fuera del if: ', musica1);

//Template Strings

const name = 'juanda';
const job = 'Web developer';

//concatenar en js forma antigua
console.log('Nombre: ' + name + ', Trabajo: ' + job);

//Concatenar en la nueva versión

console.log(`Nombre: ${name}, Trabajo: ${job}`);

//Concatenar con multiples lineas antigua

const contenedorApp = document.querySelector('#app');

let html = '<ul>' + 
                '<li> Nombre: ' + name + '</li>' +
                '<li> Trabajo: ' + job + '</li>' +
            '</ul>';

//contenedorApp.innerHTML = html;

//contenedor con multiples lineas con strings literals

let html1 = `
            <ul>
                <li> Nombre: ${name} </li>
                <li> Trabajo: ${job} </li>
            </ul>
        `;

contenedorApp.innerHTML = html1;

//CREANDO FUNCIONES
//- Function Declaration: si invocamos a la función antes de declararla esta se ejecuta
// se puede decir que el código js corre en dos partes, primero detecta el número de funciones
//que hay en el documento y luego las ejecuta.

saludar('juanda');

function saludar (name) {
    console.log(`Bienvenido ${name}`);
}

//-Function expression: si modificamos el orden la llamada nos dará error. Cuando se utiliza
//la expresión de la función de la forma mostrada abajo siempre hay que invocarlas despues y no antes.

const cliente = function (nameCustomer) {
    console.log(`Mostrando datos del cliente: ${nameCustomer}`);
}
cliente('juanda');

//Parámetros por default en las funciones
//function declaration
function actividad(nombre = 'Walter White', actividad = 'Enseñar química'){
    console.log(`La persona ${nombre}, esta realizando la actividad de ${actividad}`);
}

actividad('Juan', 'programar');
actividad('David', 'developer');
actividad('juanda');
actividad();

//function declaration
const actividad1 = function(nombre = 'Walter White', actividad1 = 'Enseñar química'){
    console.log(`La persona ${nombre}, esta realizando la actividad1 de ${actividad1}`);
}

actividad1('Juan', 'programar');
actividad1('David', 'developer');
actividad1('juanda');
actividad1();


//Arrow function

let viajando = destino => `Viajando a la ciudad: ${destino}`;

let viaje = viajando('Paris');
console.log(viaje);

//Objetos en js
//object  literal
const persona = {
    name: 'juan',
    job: 'Desarrollo web'
}

console.log(persona);

//object constructor

function Tarea(tarea, urgencia){
    this.tarea = tarea;
    this.urgencia = urgencia;
}

const tarea2 = new Tarea('preparar café', 'urgentemente');

console.log(tarea2);

//PROTOTYPE

Tarea.prototype.mostrarInfoTarea = function mostrarInformacionTarea(){
    return `La tarea ${this.tarea} tiene una prioridad de ${this.urgencia}`;
}

const tarea1 = new Tarea('Aprender js y react', 'Urgente');
console.log(tarea1);
console.log(tarea1.mostrarInfoTarea());

//Destructuring de objs

const aprendiendoJs = {
    version: {
        nueva: 'ES6',
        viaja: 'ES5'
    },
    frameworks: ['React', 'Vuejs', 'Angular']
}

//Destructuring es extraer valores de un objeto
console.log(aprendiendoJs);

//versión anterior 
//let version = aprendiendoJs.version.nueva;
//console.log(version);
//versión nueva destructuring
let {version, frameworks} = aprendiendoJs;
console.log(version, frameworks);

//object literal enhancement

const banda = 'Metallica';
const genero = 'Heavy metal';
const canciones = ['Master of puppers', 'seek & destroy'];

//forma anterior
// const metallica = {
//     banda: banda,
//     genero: genero,
//     canciones: canciones
// }

//forma nueva 

const metallica = {banda, genero, canciones}

console.log(metallica);

//metodos o funciones en un objeto

const persona2 = {
    nombre: 'juan',
    trabajo: 'desarrollador web',
    edad: 32,
    musicaRock: true,
    mostrarInformacion() {
        console.log(`${this.nombre} es ${this.trabajo} y su edad es de ${this.edad}`);
    }
}

persona2.mostrarInformacion();

//el .map conserva el arreglo origina y devuelve un arreglo nuevo.

//Objetc.keys: devuelve un array con las llaves del objeto persona2

console.log(Object.keys(persona2));

//spread operatos ...

let lenguajes = ['js', 'PHP', 'Python'];
let frameworks1 = ['Rjs', 'Laravel', 'Django'];

//unir los arrays en uno solo.
let combinacion = [...lenguajes, ...frameworks1];
console.log(combinacion);

//PROMISES

const aplicarDescuento = new Promise((resolve, reject) => {
    setTimeout( () => {
        let descuento = true;

        descuento ? resolve('Descuento aplicado!') : reject('No se pudo aplicar el descuento')

    }, 4000);
});

aplicarDescuento.then(response => {
    console.log(response);
});

//PROMISES AJAX

const descargarUsuarios = cantidad => new Promise((resolve, reject) => {
    //pasar la cantidad a a api

    const api = `https://randomuser.me/api/?results=${cantidad}&nat=us`;
    

    //llamado a ajax
    const xhr = new XMLHttpRequest();
    
    //abrir la conexión
    xhr.open('GET', api, true);

    // on load
    xhr.onload = () => {
        xhr.status === 200 ? resolve(JSON.parse(xhr.responseText).results) : reject(Error(xhr.statusText));
    }

    //opcional (on error)
    xhr.onerror = (error) => reject(error); 

    //send
    xhr.send();

});

descargarUsuarios(28).then(resultado => {
    console.log(resultado);
});

//POO CLASES

class Tarea2 {
    constructor(nombre, prioridad){
        this.nombre = nombre;
        this.prioridad = prioridad;
    }

    mostrar(){
        return `${this.nombre} tiene una prioridad de ${this.prioridad}`;
    }
}

class ComprasPendientes extends Tarea2 {
    constructor(nombre, prioridad, cantidad) {
        super(nombre, prioridad);
        this.cantidad = cantidad;
    }

    mostrar(){
        super.mostrar();
        console.log(`y la cantidad de ${this.cantidad}`);
    }

    hola(){
        return 'hola';
    }
}

//creamos los objetos
let tarea3 = new Tarea2('Aprender React', 'Alta');
let tarea21 = new Tarea2('Sacar a Junior', 'Alta');

console.log(tarea3.mostrar());
console.log(tarea21.mostrar());

//compras

let compra1 = new ComprasPendientes('juanda', 'Alta', 3);
compra1.mostrar();
console.log('undefined', compra1.hola());

//MODULOS EN ES6







